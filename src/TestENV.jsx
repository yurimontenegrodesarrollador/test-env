import { useState, useEffect } from "react";

const TestENV = () => {
  const [data, setData] = useState(null);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_TEST}/company/ym`)
      .then((res) => res.json())
      .then((dat) => setData(dat))
      .catch((el) => setData(el));
  }, []);

  return (
    <div>
      <h4>INFORMACIÓN DE PRUEBA</h4>
      <hr />
      {JSON.stringify(data)}
      <hr />
    </div>
  );
};

export default TestENV;

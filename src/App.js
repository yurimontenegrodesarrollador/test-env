import React, { useState } from "react";
import TextField from "@material-ui/core/TextField";
import AdapterDateFns from "@material-ui/lab/AdapterDateFns";
import LocalizationProvider from "@material-ui/lab/LocalizationProvider";
import DateRangePicker from "@material-ui/lab/DateRangePicker";
import { Box, Container, Card, CardContent } from "@material-ui/core";
import confetti from "canvas-confetti";
import Carousel from "react-tiny-slider";
import SunEditor from "suneditor-react";
import "suneditor/dist/css/suneditor.min.css";
import TestENV from "./TestENV";

function App() {
  const carousel = React.useRef(null);
  var duration = 15 * 1000;
  var animationEnd = Date.now() + duration;
  var defaults = { startVelocity: 30, spread: 360, ticks: 60, zIndex: 0 };

  const [dataEditor, setDataEditor] = useState("");
  console.log(dataEditor);
  function randomInRange(min, max) {
    return Math.random() * (max - min) + min;
  }

  var interval = setInterval(function () {
    var timeLeft = animationEnd - Date.now();

    if (timeLeft <= 0) {
      return clearInterval(interval);
    }

    var particleCount = 50 * (timeLeft / duration);
    // since particles fall down, start a bit higher than random
    confetti(
      Object.assign({}, defaults, {
        particleCount,
        origin: { x: randomInRange(0.1, 0.3), y: Math.random() - 0.2 },
      })
    );
    confetti(
      Object.assign({}, defaults, {
        particleCount,
        origin: { x: randomInRange(0.7, 0.9), y: Math.random() - 0.2 },
      })
    );
  }, 250);

  const [value, setValue] = React.useState([null, null]);

  React.useEffect(() => {
    setInterval(() => carousel.current.goTo("next"), 3000);
  }, [carousel]);

  return (
    <>
      <Container maxWidth="sm">
        <Carousel
          swipeAngle={false}
          items={1}
          mouseDrag
          ref={carousel}
          controls={false}
          nav={false}
        >
          {[1, 2, 3, 4, 5].map((slide) => (
            <Card style={{ border: "1px solid red" }}>
              <CardContent>
                <div>{slide}</div>
              </CardContent>
            </Card>
          ))}
        </Carousel>
      </Container>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <DateRangePicker
          value={value}
          onChange={(newValue) => {
            setValue(newValue);
          }}
          renderInput={(startProps, endProps) => (
            <React.Fragment>
              <TextField {...startProps} label="Fecha inicio" />
              <Box sx={{ mx: 2 }}> to </Box>
              <TextField {...endProps} label="Fecha fin" />
            </React.Fragment>
          )}
        />
      </LocalizationProvider>
      <div style={{ border: "1px dashed #aaa", padding: 20 }}>
        <TestENV />
      </div>

      <div style={{ border: "1px solid skyblue" }}>
        <SunEditor
          setOptions={{
            height: "500px",
            buttonList: [
              [
                "undo",
                "redo",
                "codeView",
                "save",
                "showBlocks",
                "fullScreen",
                "preview",
                "print",
              ],
              [
                "formatBlock",
                "font",
                "fontSize",
                "fontColor",
                "bold",
                "hiliteColor",
                "underline",
                "italic",
                "strike",
                "subscript",
                "superscript",
                "link",
                "outdent",
                "indent",
              ],
              ["horizontalRule", "list", "align", "table", "removeFormat"],
              ["image", "video"],
            ],
          }}
          setAllPlugins={true}
          onChange={(event) => setDataEditor(event)}
        />
      </div>
    </>
  );
}

export default App;
